from collections import OrderedDict
n=int(input())
order_dict=OrderedDict()
for i in range(n):
    litem=input().split()
    item=' '.join(litem[:-1])
    price=int(litem[-1])
    if order_dict.get(item):
        order_dict[item]+=price
    else:
        order_dict[item]=price
for i,p in order_dict.items():
    print(i,p)
