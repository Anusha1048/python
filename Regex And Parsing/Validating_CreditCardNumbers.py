import re
n=int(input())
for i in range(n):
    s=input()
    valid_num=bool(re.match(r'[456]([\d]{15}|[\d]{3}(-[\d]{4}){3}$)',s))
    s1=s.replace("-","")
    repeat_num=re.search(r"([\d])\1\1\1", s1)
    if valid_num and not repeat_num: 
        print('Valid')
    else:
        print('Invalid')
