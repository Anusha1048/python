import re
n=int(input())
def Validate(uid):
    two_upper=bool(re.search(r'.*([A-Z].*){2,}',uid))
    three_digits=bool(re.search(r'.*([0-9].*){3,}',uid))
    ten_elements=bool(re.search(r'([a-zA-Z0-9]){10}$',uid))
    repeat_letter=bool(re.search(r'.*(.).*\1',uid))
    if two_upper and three_digits and ten_elements and not repeat_letter:
        return "Valid"
    else:
        return "Invalid"
for i in range(n):
    print(Validate(input()))
