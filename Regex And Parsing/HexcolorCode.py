import re
n=int(input())
for i in range(n):
    s=input()
    hex_code=re.findall(r"[\s|:](#[a-fA-F0-9]{6}|#[a-fA-F0-9]{3})",s)
    if hex_code:
        print(*hex_code, sep='\n')
